﻿using CorePhish.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorePhish.Data
{
    public class PhisherContext : DbContext 
    {
        public PhisherContext(DbContextOptions<PhisherContext> opt) : base(opt)
        {
                 
        }

        public DbSet<Phisher> Phishers { get; set; }

    }
}
