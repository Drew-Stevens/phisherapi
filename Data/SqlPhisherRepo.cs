﻿using CorePhish.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorePhish.Data
{
    public class SqlPhisherRepo : ICorePhishRepo
    {
        private readonly PhisherContext _context;

        public SqlPhisherRepo(PhisherContext context)
        {
            _context = context;
        }

        public void CreatePhisher(Phisher phish)
        {
            if(phish == null)
            {
                throw new ArgumentNullException(nameof(phish));
            }
            _context.Phishers.Add(phish);
        }

        public IEnumerable<Phisher> GetAllPhishers()
        {
            return _context.Phishers.ToList();
        }

        public Phisher GetPhisherById(int id)
        {
            return _context.Phishers.FirstOrDefault(u => u.Id == id);
        }

        public bool SaveChanges()
        {
           return  (_context.SaveChanges() >=0);
        }

        public void UpdatePhisher(Phisher phish)
        {
            
        }
    }
}
