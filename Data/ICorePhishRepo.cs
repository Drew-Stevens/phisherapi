﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorePhish.Models;


namespace CorePhish.Data
{
    public interface ICorePhishRepo
    {
        bool SaveChanges();
        IEnumerable<Phisher> GetAllPhishers();
        Phisher GetPhisherById(int id);
        void CreatePhisher(Phisher phish);
        void UpdatePhisher(Phisher phish);
    }
}
