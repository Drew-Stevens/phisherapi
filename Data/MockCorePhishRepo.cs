﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CorePhish.Models;

namespace CorePhish.Data
{
    public class MockCorePhishRepo : ICorePhishRepo
    {
        public void CreatePhisher(Phisher phish)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Phisher> GetAllPhishers()
        {
            var phishers = new List<Phisher>
            {
                new Phisher{Id=0, Name="UPS", Level=3},
                new Phisher{Id=1, Name="Login", Level=2},
                new Phisher{Id=2, Name="Hack", Level=1}
            };

            return phishers;
        }

        public Phisher GetPhisherById(int id)
        {
            return new Phisher { Id = 0, Name = "UPS", Level = 3 };
        }

        public bool SaveChanges()
        {
            throw new NotImplementedException();
        }

        public void UpdatePhisher(Phisher phish)
        {
            throw new NotImplementedException();
        }
    }
}
