﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorePhish.Dtos
{
        public class PhisherReadDto
        {
            public int Id { get; set; }
            public int Level { get; set; }
            public string Name { get; set; }
        }
    

}
