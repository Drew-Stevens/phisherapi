﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CorePhish.Dtos
{
    public class PhisherCreateDto
    {
        [Required]
        public int Level { get; set; }
        [Required]
        [MaxLength(250)]
        public string Name { get; set; }
        public string FileLocation { get; set; }
   }
}
