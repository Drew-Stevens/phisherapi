﻿using AutoMapper;
using CorePhish.Dtos;
using CorePhish.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorePhish.Profiles
{
    public class PhishersProfile : Profile
    {
        public PhishersProfile()
        {
            //Source -> Target
            CreateMap<Phisher, PhisherReadDto>();
            CreateMap<PhisherCreateDto, Phisher>();
        }
    }
}
