﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CorePhish.Data;
using CorePhish.Models;
using AutoMapper;
using CorePhish.Dtos;

namespace CorePhish.Controllers
{
    [Route("api/phishers")]
    [ApiController]
    public class PhishersController : ControllerBase
    {
        private readonly ICorePhishRepo _repository;
        private readonly IMapper _mapper;

        public PhishersController(ICorePhishRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        //private readonly MockCorePhishRepo _repository = new MockCorePhishRepo();

        //GET api/phishers
        [HttpGet]
        public ActionResult <IEnumerable<PhisherReadDto>> GetAllPhishers()
        {
            var phishItems = _repository.GetAllPhishers();

            return Ok(_mapper.Map<IEnumerable<PhisherReadDto>>(phishItems));
        }

        //GET api/phishers/id such as 3 for example, get exact one
        [HttpGet("{id}", Name = "GetPhisherById")]
        public ActionResult<Phisher> GetPhisherById(int id)
        {
            var phishItem = _repository.GetPhisherById(id);
            if (phishItem != null)
            {
                return Ok(_mapper.Map<PhisherReadDto>(phishItem));
            }
            return NotFound();
        }

        [HttpPost]
        public ActionResult <PhisherReadDto> CreatePhisher(PhisherCreateDto phisherCreateDto)
        {
            var phisherModel = _mapper.Map<Phisher>(phisherCreateDto);
            _repository.CreatePhisher(phisherModel);
            _repository.SaveChanges();

            var phisherReadDto = _mapper.Map<PhisherReadDto>(phisherModel);

            return CreatedAtRoute(nameof(GetPhisherById), new { Id = phisherReadDto.Id }, phisherReadDto);
        }

    }
}
